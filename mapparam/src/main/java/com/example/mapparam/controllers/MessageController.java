package com.example.mapparam.controllers;

import com.example.mapparam.DTO.ParamDTO;
import com.example.mapparam.service.ParamBusinessImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MessageController {
    @Autowired
    ParamBusinessImpl paramBusinessImpl;
    @GetMapping("/SMS")
    public String messageBO(@RequestParam(defaultValue = "0") String contentCode, @RequestBody ParamDTO paramDTO){
        try {
           String content= paramBusinessImpl.replaceParam(paramDTO,contentCode);
           return content;
        }
        catch (Exception e){
            return e.getMessage();
        }
    }
}
