package com.example.mapparam.repository;

import com.example.mapparam.models.Param;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParamRepository extends JpaRepository<Param,Long> {
}
