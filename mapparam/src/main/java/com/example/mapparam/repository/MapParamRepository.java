package com.example.mapparam.repository;

import com.example.mapparam.models.MapParam;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MapParamRepository extends JpaRepository<MapParam,Long> {
}
