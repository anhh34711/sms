package com.example.mapparam.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParamDTO {
    private String numDay;
    private String packagePercent;
    private String totalMoney;
}

