package com.example.mapparam.service;

import com.example.mapparam.DTO.ParamDTO;
import com.example.mapparam.models.Content;
import com.example.mapparam.repository.ContentRepository;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

@Service
public class ParamBusinessImpl {
    @Autowired
    ContentRepository contentRepository;
    private String convertParamToTypeDB(String contentParam) {
        StringBuilder stringBuilder = new StringBuilder(contentParam);
        for (int i = 0; i < contentParam.length(); i++) {
            if (Character.isUpperCase(contentParam.codePointAt(i))) {
                stringBuilder.replace(0, stringBuilder.length(),
                        contentParam.replaceAll(contentParam.charAt(i) + "", "_"
                                + Character.toLowerCase(contentParam.charAt(i))));
            }
        }
        stringBuilder.append("%");
        stringBuilder.insert(0, "%");
        return stringBuilder.toString();
    }

    public String replaceParam( ParamDTO convertToDbDTO,String contentId) throws  InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        if(!StringUtils.isNumeric(contentId)){
            throw new NullPointerException("code content invalid");
        }
        Content content=contentRepository.findById(Long.parseLong(contentId)).orElse(null);
        if(content==null){
            throw new NullPointerException("code content invalid");
        }
        String contentRoot=content.getContent();
        StringBuilder stringBuilder = new StringBuilder(contentRoot);
        for (Field field : ParamDTO.class.getDeclaredFields()) {
            String root1 = convertParamToTypeDB(field.getName());
            if (contentRoot.contains(root1)) {
                if(BeanUtils.getProperty(convertToDbDTO,field.getName())==null){
                   throw new NullPointerException("field data can't empty");
                }
                stringBuilder.replace(0, stringBuilder.length(),
                        stringBuilder.toString().replace(root1, BeanUtils.getProperty(convertToDbDTO,field.getName())));
            }
        }
        return stringBuilder.toString();
    }

}

