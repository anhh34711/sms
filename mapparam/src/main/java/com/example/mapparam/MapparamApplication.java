package com.example.mapparam;

import com.example.mapparam.service.ParamBusinessImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapparamApplication {
    @Autowired
    ParamBusinessImpl convertToDbBusiness;
    public static void main(String[] args) {
        SpringApplication.run(MapparamApplication.class, args);
    }

}
