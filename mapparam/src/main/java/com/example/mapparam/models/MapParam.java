package com.example.mapparam.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.sql.DataSourceDefinition;
import javax.persistence.*;

@Entity
@Table(name = "map_param")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MapParam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long paramId;
    private long contentId;
}
